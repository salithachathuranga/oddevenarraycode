package com.nCinga;

public class Main {

    public static void getSequence (int[] numbers) {

        int[] currentArray = new int[numbers.length];
        int[] resultArray = new int[numbers.length];
        int currentArrayCount = 0;
        int resultArrayCount = 0;

        boolean flag = false;

        if(numbers.length == 1){
            System.out.println(numbers[0]);
        }
        else{
            for(int iteration = 0; iteration < numbers.length-1; iteration++){

                currentArray[currentArrayCount] = numbers[iteration];
                if(
                    (currentArray[currentArrayCount]%2 == 0 && numbers[iteration+1]%2 != 0) ||
                    (currentArray[currentArrayCount]%2 != 0 && numbers[iteration+1]%2 == 0)
                ){
                    currentArray[currentArrayCount+1] = numbers[iteration+1];
                    currentArrayCount += 1;

                }
                else{
                    flag = true;
                    if(resultArrayCount < currentArrayCount){
                        resultArrayCount = currentArrayCount;

                        for(int i = 0; i <= currentArrayCount; i++){
                            resultArray[i] = currentArray[i];
                        }
                        currentArrayCount = 0;

                    }
                }
            }
            if(!flag){
                for(int j = 0; j <= currentArrayCount; j++){
                    resultArray[j] = currentArray[j];
                }
                for(int loop = 0; loop <= currentArrayCount; loop++){
                    System.out.print(resultArray[loop] + " ");
                }
            }
            else {
                for(int loop = 0; loop < resultArrayCount; loop++){
                    System.out.print(resultArray[loop] + " ");
                }
            }
        }

    }

    public static void main(String[] args) {
        int[] input = {1, 2, 3, 4, 5, 6, 7, 8, 9, 4, 2, 7, 4 , 1, 8, 1, 2, 3, 4, 5, 6, 7, 8, 9, 4, 6 ,7, 8, 3};
        getSequence(input);
    }
}
